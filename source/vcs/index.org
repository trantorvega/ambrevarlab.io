#+TITLE: Version Control System: The One Computer Skill
#+DATE: <2016-09-28 Wed>

The first thing we should teach about driving a vehicle is where the brakes are.
I believe that the same reasoning applies to computers.  Before teaching any
sort of text/web editing or programming, wouldn't it be reasonable to teach how
to /save your work/?

There is something essential that could be taught first hand and which I believe
to be the number one skill that will remain invaluable for the rest of your life
as a computer user: /version control systems/ (VCS).

From [[https://en.wikipedia.org/w/index.php?title=Version_control&oldid=740941888][Wikipedia]]:

#+BEGIN_QUOTE
Version control […] is the management of changes to documents, computer
programs, large web sites, and other collections of information.
#+END_QUOTE

For a concrete and straightforward example, look at the [[https://en.wikipedia.org/w/index.php?title=Version_control&action=history][history]] of a Wikipedia
page.

A VCS is:

- a glue to collaboration,
- a foundation to cloud-computing,
- the ultimate backup,
- a looking glass to the past.

Are you a book or a blog author?  A scientist?  An administrator?  An accountant?
If you find yourself writing and maybe collaborating with text documents in any
form, a VCS will be your best friend.

When computer users are not aware of the existence of VCSes, they are left alone
with poor tools for tasks as simple as saving and collaborating, which yields to
a lot of frustration.  A common example would be exchanging files via e-mail:

- It is cumbersome to send or save the attachments every time.
- It generates confusion in the versions.
- Conflict resolution is a nightmare.  (When 2 e-mails are cross-sent.)

A few free, decentralized and popular VCS include:

- [[https://git-scm.com][git]]
- [[https://www.mercurial-scm.org/][mercurial]] (hg)
- [[http://bazaar.canonical.com/][bazaar]] (bzr)

A VCS can hold much of your project information:

- history of changes,
- version tags,
- authoring.

Thus the VCS metadata should be stored in an /open format/, that is, in a format
that is open for viewing and editing at any time in the future.  Should the VCS
become un-maintained, the OS support be dropped, or any other catastrophe occur,
the project information can be recovered.

VCSes should be decentralized. See this [[http://www.youtube.com/watch?v=4XpnKHJAok8][Tech Talk: Linux Torvalds on git]] for a
rationale.

Other features:

- Permanent online access for files hosted on a server (public or private).
- Offline access: decentralization means you do not need to be connected to
  perform any sort of VCS management.
- Content guarantee: content is check-summed, which means that no
  data-corruption is left unnoticed.
- Show differences across versions.
- Handle conflicts.
- Fine control of the content that is under version control: no spurious file
  (temp file), no wrong save of a file, clear view at what is being synced (up
  and down).

Popular sharing services like Dropbox suffer from the terrible drawback of
hiding the syncing operation details: when offline, if you change the content
then go online again, what happens?  Will Dropbox discard the changes or upload
them?  What if there is a conflict, i.e. some file got changed in the Dropbox
repository in the meantime?

If you care about the content of your files, do not use such tools.  It is
essential to have full control over every VCS operation.

There is one pitfall with VCSes: once you have started working in a team, the
VCS cannot (or at least not so easily) be changed.  And everybody has to abide
by the choice of the VCS.  It is the only tool that team-members are forced
using and must be agreed upon; the users are free to use their own favorite
tools for the rest.  This as opposed to, say, Google Documents where the full
tool-suite is bundled into one service: the web profile, the VCS, the document
processor, the editor, etc.

Not all computer data is "versionable" though.  Everything (pictures, videos,
music) can be uploaded to a VCS server, but to make the best out of your VCS the
content should be in text format, so that the VCS can display the differences
between versions.  Besides, it is far more efficient in terms of disk usage and
will save a lot on bandwidth.

Avoid =.docx=, =.odt= or other *Office documents: they are archived-files, a VCS
will treat them as raw data, it won't be able to "see" the textual content.

For book or blog authoring, prefer text formats such as [[https://orgmode.org/][Org]], [[http://daringfireball.net/projects/markdown/][Markdown]] or [[https://www.latex-project.org/][LaTeX]].

Text-formats can have some more exotic uses:

- Spreadsheets with Org-mode.
- Vector graphics (e.g. with [[http://asymptote.sourceforge.net/][Asymptote]]: for geomatrical or scientific drawings, this
  is much more efficient than drawings by hand with [[https://inkscape.org/][Inkscape]] and the like.

Last but not least, a VCS can be somewhat technical and challenging to master,
especially when working in a group.  Which is why I believe a proper
introductory course is necessary.
