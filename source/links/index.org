#+TITLE: Links and literature
#+DATE: <2014-11-11 Tue>

Here follows a collection of articles, books and videos that I'd personally
recommend…

…Or not!  Because I believe it's also important to cover a spectrum of
literature wider than just someone's own opinion.  Many links refer to arguments
and counter-arguments: I don't necessarily agree with everything listed here.
Nonetheless I believe it's wiser to not automatically discard antagonistic
opinions.  They can prove instrumental in further consolidating one's argument.

Besides, I've spent a significant amount of time reading books which, with
hindsight, I now consider a waste of time.  If sharing my opinions can help
people on the same page avoid wasting their own time, then for the better!

Discussion is more than welcome!

# I format links as level-4 headers so that they don't appear in the table of
# contents, they don't display too large and they can be tagged.

* Political science
**** [[https://en.wikipedia.org/wiki/The_Dictator's_Handbook][The Dictator's Handbook]], by /Bruce Bueno de Mesquita and Alastair Smith/ :politic:
See these videos for a short introduction to the selectorate theory:
- [[https://www.youtube.com/watch?v=rStL7niR7gs][The Rules for Rulers]]
- [[https://www.youtube.com/watch?v=rStL7niR7gs][Death & Dynasties (Rules for Rulers Follow-up)]]
**** [[https://en.wikipedia.org/wiki/Paradise_Papers][Paradise Papers]]                                                :politic:
**** [[https://themountainshadowebook.com/interview.html][GDR's Final Interview]]
**** [[https://www.jofreeman.com/joreen/tyranny.htm][The Tyranny of Stucturelessness]], by /Jo Freeman/         :politic:essay:
* Economy
**** [[https://www.gapminder.org/factfulness/][Factfulness]], by /Hans Rosling/                                 :economy:
**** [[https://www.gapminder.org/][Gapminder]]                                                      :economy:
**** [[https://www.gapminder.org/videos/dont-panic-end-poverty][Don’t Panic — End Poverty]], by /Hans Rosling/             :video:economy:
**** [[http://www.paulgraham.com/gap.html][Mind the Gap]]                                                   :economy:
**** [[http://paulgraham.com/wealth.html][How to Make Wealth]]                                             :economy:
**** [[http://www.paulgraham.com/softwarepatents.html][Are Software Patents Evil?]]                             :economy:patents:
**** L'économie du bonheur, by /Claudia Senik/                      :economy:
* Anthropology
**** Sapiens, by /Yuval Noah Harari/                                   :book:
**** [[https://www.youtube.com/watch?v=g6BK5Q_Dblo][New religions of the 21st century]]                     :psychology:video:
* History
**** 1984, by /George Orwell/                               :history:politic:
**** [[https://slate.com/technology/2019/10/consequential-computer-code-software-history.html][The Lines of Code That Changed Everything]]            :history:computing:
* Linguistic
**** [[http://www.orwell.ru/library/essays/politics/english/e_polit/][Politics and the English language]], by /George Orwell/            :essay:
* Education and psychology
**** [[https://www.ted.com/talks/ken_robinson_says_schools_kill_creativity][Do schools kill creativity?]], by /Ken Robinson/         :video:education:
**** [[https://www.ted.com/talks/hans_rosling_shows_the_best_stats_you_ve_ever_seen][The best stats you've ever seen]], by /Hans Rosling/ :politic:psychology:video:economy:
**** [[https://www.ted.com/talks/elizabeth_pisani_sex_drugs_and_hiv_let_s_get_rational_1][Sex, drugs and HIV -- let's get rational]], by /Elizabeth Pisani/ :video:education:psychology:
**** [[https://www.edutopia.org/textbook-publishing-controversy][A Textbook Example of What's Wrong with Education]]            :education:
**** [[http://www.youtube.com/watch?v=EYPapE-3FRw][Feynman on the scientific method]]                             :education:
**** [[https://en.wikipedia.org/wiki/List_of_cognitive_biases][List of cognitive biases]]                                    :psychology:
**** [[https://en.wikipedia.org/wiki/The_Man_Who_Mistook_His_Wife_for_a_Hat][The Man Who Mistook His Wife for a Hat]], by /Oliver Sacks/   :psychology:
**** [[https://en.wikipedia.org/wiki/Mirror_writing][Mirror writing]]                                              :psychology:
**** [[http://youarenotsosmart.com/][You Are Not So Smart]]                                   :book:psychology:
***** [[https://youarenotsosmart.com/2017/07/20/102-weird-science-rebroadcast/][YANSS 102 – WEIRD Science]]                                  :psychology:
***** [[https://youarenotsosmart.com/2017/09/11/yanss-110-how-sleep-deprivation-dulls-our-minds-and-unleashes-our-biases][YANSS 110 – How sleep deprivation dulls our minds and unleashes our biases]] :psychology:
One of my all-time favourites!
***** [[http://youarenotsosmart.com/2018/02/26/yanss-122-how-our-unchecked-tribal-psychology-pollutes-politics-science-and-just-about-everything-else/][YANSS 122 – How our unchecked tribal psychology pollutes politics, science, and just about everything else]] :psychology:
**** Thinking, Fast and Slow, by /Daniel Kahnemann/              :psychology:
**** [[http://freakonomics.com/archive/][Freakonomics]]                         :psychology:education:book:economy:
**** [[http://www.paulgraham.com/hs.html][What You'll Wish You'd Known]], by /Paul Graham/    :education:psychology:
**** [[http://paulgraham.com/say.html][What You Can't Say]], by /Paul Graham/                        :psychology:
**** [[http://www.paulgraham.com/disagree.html][How to Disagree]], by /Paul Graham/                   :education:internet:
**** [[http://www.paulgraham.com/nerds.html][Why Nerds are Unpopular]], by /Paul Graham/         :education:psychology:
**** [[http://www.paulgraham.com/lies.html][Lies We Tell Kids]], by /Paul Graham/               :education:psychology:
**** [[https://medium.com/how-to-fly-a-horse/the-dumbest-question-you-can-ask-a-scientist-fb98d34aaf40][The Dumbest Question You Can Ask a Scientist]]                 :education:
**** [[http://steve-yegge.blogspot.fr/2008_06_01_archive.html][Done, and get things smart]], by /Steve Yegge/      :education:psychology:
**** [[https://en.wikipedia.org/wiki/Mental_abacus][Mental abacus]]                                                :education:
**** [[http://nic.ferrier.me.uk/blog/2013_08/lets-not-code][let's not learn to code]], by /Nic Ferrier/          :education:computing:
**** [[https://en.wikipedia.org/wiki/List_of_common_misconceptions][List of common misconceptions]]                                :education:

* Psychology in computer programming
**** [[http://bikeshed.com/][Bikeshed]]                                :psychology:education:computing:
**** [[http://xyproblem.info/][The XY problem]]                          :psychology:education:computing:
**** [[https://www.chiark.greenend.org.uk/~sgtatham/bugs.html][How to Report Bugs Effectively]]          :psychology:education:computing:
**** [[http://www.catb.org/~esr/faqs/smart-questions.html][How To Ask Questions The Smart Way]]      :psychology:education:computing:
**** [[https://sealedabstract.com/rants/conduct-unbecoming-of-a-hacker/][Conduct unbecoming of a hacker]]          :psychology:education:computing:
**** [[http://www.cs.nott.ac.uk/~cah/G51ISS/Documents/NoSilverBullet.html][No Silver Bullet]], by /Frederick P. Brooks, Jr./ :book:psychology:education:computing:
This chapter was included in the [[https://en.wikipedia.org/wiki/The_Mythical_Man-Month][The Mythical Man-Month]] book.

**** [[https://blog.codinghorror.com/welcome-to-the-tribe/][Welcome to the Tribe]]                              :psychology:computing:
**** [[http://norvig.com/21-days.html][Teach Yourself Programming in Ten Years]], by /Peter Norvig/ :psychology:computing:

* Social sciences and computing
**** [[http://www.catb.org/esr/writings/cathedral-bazaar/][The Cathedral and the Bazaar]], by /Eric S. Raymond/        :book:freedom:
This is a short read that makes an interesting case of free software
development.

I don't really like the analogy since it may imply that closed software
development can build grand programs that are out of reach to the free software
model.
**** [[http://keithcu.com/wordpress/?page_id=407][After the Software Wars]]                                           :book:
This book has a surprising structure: while about a bit more than the first
half is an interesting discussion over freedom and ethic, the last part gets
lost in a US political rant.  I think it's safe to drop it after reaching this
last part.
**** http://www.patentcountdown.org
**** [[https://www.outreachy.org/][Outreachy]]
**** [[https://www.contributor-covenant.org/][Contributor Covenant]]
**** [[http://daniellakens.blogspot.com/2017/04/five-reasons-blog-posts-are-of-higher.html][Five reasons blog posts are of higher scientific quality than journal articles]]
**** [[http://email.is-not-s.ms/][email.is-not-s.ms]]                                                :email:

* Security and communication
**** [[https://www.nytimes.com/2012/01/05/opinion/internet-access-is-not-a-human-right.html?pagewanted=all][Internet Access Is Not a Human Right]], by /Vinton G. Cerf/ :security:social:internet:politic:freedom:
**** [[https://www.ted.com/talks/edward_snowden_here_s_how_we_take_back_the_internet][Edward Snowden: Here's how we take back the Internet]] :video:freedom:internet:politic:
**** [[https://www.ted.com/talks/glenn_greenwald_why_privacy_matters][Glenn Greenwald: Why privacy matters]]            :video:politic:internet:
**** [[https://www.youtube.com/watch?v=efs3QRr8LWw][Joe Rogan Experience #1368 - Edward Snowden]]                 :algorithms:
**** [[https://en.wikipedia.org/wiki/Permanent_Record_(autobiography)][Permanent Record]], by /Edward Snowden/            :politic:internet:book:
**** [[https://www.ted.com/talks/andy_yen_think_your_email_s_private_think_again][Andy Yen: Think your email's private? Think again]] :security:email:video:
**** [[https://ssd.eff.org/][Surveillance Self-Defense]]                            :security:internet:
***** [[https://ssd.eff.org/en/module/animated-overview-how-make-super-secure-password-using-dice][How to Make a Super-Secure Password Using Dice]] :security:internet:cryptography:
**** [[https://emailselfdefense.fsf.org/en/][Email Self-Defense]]                                      :email:security:
**** [[http://coding2learn.org/blog/2014/04/14/please-stop-sending-me-your-shitty-word-documents][Please stop sending me your shitty Word documents]]        :freedom:email:
**** [[https://lwn.net/Articles/702177/][Why kernel development still uses email]]                          :email:
**** [[https://en.wikipedia.org/wiki/Cold_boot_attack][Cold boot attack]]                                              :security:
**** [[https://www.youtube.com/watch?v=HUVmypx9HGI][IPFS and the Permanent Web]]           :internet:freedom:politic:security:

* Broken security
**** [[https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/][How to Use Signal Without Giving Out Your Phone Number]]        :security:
Signal security model is seriously flawed.  For one, the security model is not
in the user's control.  Our own data does not belong to us.
This is also valid for Whatsapp.
**** [[https://www.quora.com/What-makes-BlackBerry-more-secure-than-the-others][What makes BlackBerry more secure than the others?]]            :security:
Not much, if not nothing at all.  The diagram speaks for itself, Blackberry has
(or had?) full access to the users un-encrypted data.
**** [[http://phrack.org/][Phrack]]                                                        :security:

* Free software philosophy
**** [[https://www.gnu.org/gnu/thegnuproject.html][The GNU project]]                                                :freedom:
**** [[https://www.fsf.org/bulletin/2013/fall/how-can-free-software-protect-us-from-surveillance][How can free software protect us from surveillance?]] :politic:freedom:internet:
**** [[https://www.gnu.org/philosophy/open-source-misses-the-point.html][Why Open Source misses the point of Free Software]]              :freedom:
**** [[https://www.gnu.org/philosophy/stallman-kth.html][RMS lecture at KTH (Sweden)]], 30 October 1986                   :freedom:
**** [[https://github.com/dear-github/dear-github][Dear GitHub]]                                                    :freedom:
**** [[https://mikegerwitz.com/2015/05/Gitlab-Gitorious-and-Free-Software][Gitlab, Gitorious, and Free Software]]                  :freedom:internet:
**** [[https://mikegerwitz.com/2016/01/Google-Analytics-Removed-from-GitLab.com-Instance][Google Analytics Removed from GitLab.com Instance]]     :freedom:internet:
**** [[https://commandcenter.blogspot.in/2006/06/i-cant-find-this-on-web-so-here.html][Rob Pike vs. Richard Stallman]]                                  :patents:

* Decentralized Internet
**** [[https://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/?single_page=true][As We May Think]] by /Vannevar Bush/                            :internet:
The idea of Wikipedia may be as old as 1945.
**** [[https://the-federation.info/][The Federation - Decentralized social network statistics]]        :social:

* Hardware and freedom
**** [[https://h-node.org/][h-node]]                                                :hardware:freedom:
**** [[https://store.vikings.net/][Vikings]]                                               :hardware:freedom:
**** [[https://puri.sm/][Purism]]                                                :hardware:freedom:
**** [[https://secure.raptorcs.com/][Raptor computing systems]]                                      :hardware:
**** [[https://www.phoronix.com/][Phoronix]]
**** [[http://en.tldp.org/HOWTO/Unix-Hardware-Buyer-HOWTO][The Unix Hardware Buyer HOWTO]]                 :history:hardware:freedom:
This is now rather outdated but still an interesting read for historical
purposes.

* Tools and practices for hackers
**** [[https://en.wikipedia.org/wiki/Leaky_abstraction][The Law of Leaky Abstractions]]
**** [[https://www.joelonsoftware.com/articles/Unicode.html][The Absolute Minimum Every Software Developer Absolutely, Positively Must Know About Unicode and Character Sets (No Excuses!)]]
**** [[https://stallman.org/stallman-computing.html][How I do my Computing]], by /Richard M. Stallman/
**** [[https://www.kernel.org/doc/Documentation/CodingStyle][Linux kernel coding style]]
**** [[https://usesthis.com/interviews/greg.kh][Uses This / Greg Kroah-Hartman]]
**** [[http://www.youtube.com/watch?v=4XpnKHJAok8][Tech Talk: Linux Torvalds on git]]
**** [[https://github.com/erlang/otp/wiki/writing-good-commit-messages][Writing good commit messages]]
**** [[http://freshmeat.sourceforge.net/articles/stop-the-autoconf-insanity-why-we-need-a-new-build-system][Stop the autoconf insanity! Why we need a new build system.]]
**** [[http://semver.org][Semantic Versioning]]
**** [[http://rosettacode.org/wiki/Rosetta_Code][Rosetta Code]]
**** [[https://dotfiles.github.io][dotfiles]]
**** [[http://www.linusakesson.net/programming/syntaxhighlighting/][A case against syntax highlighting]]
**** [[https://www.darkcoding.net/software/facebooks-code-quality-problem/][Facebook’s code quality problem]]
**** [[https://archive.eiffel.com/doc/manuals/technology/bmarticles/uml/page.html][UML: The Positive Spin]]

* Operating systems of the future
**** [[https://nixos.org/~eelco/pubs/phd-thesis.pdf][The Purely Functional Software Deployment Model - NixOS]]        :os:book:
**** [[https://www.gnu.org/software/guix/blog/2018/tarballs-the-ultimate-container-image-format/][Tarballs, the ultimate container image format]]
**** [[https://reproducible-builds.org/][Reproducible builds]]                                :freedom:security:os:
**** https://repology.org/
**** [[https://seravo.fi/2013/seven-reasons-why-tablets-are-the-future][Seven reasons why tablets are the future]]
**** [[https://www.cs.jhu.edu/~seaborn/plash/html/index.html][Plash: the Principle of Least Authority shell]]           :shell:security:
**** [[http://futurist.se/gldt/][Linux distribution timeline]]
Also see [[https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg][Linux distribution timeline on Wikipedia]].

* Operating system of the past: Unix
While Unix brought us lots of good ideas, I also beieve many things are wrong
with Unix and more generally with the Unix philosophy.  The discussion is rather
complex so I won't go into details here.  Let's keep it for a dedicated article.

**** [[https://stallman.org/articles/posix.html][The origin of the name POSIX.]]                             :unix:history:
**** [[http://web.mit.edu/%7Esimsong/www/ugh.pdf][The Unix-Haters Handbook]]                                          :book:
***** [[http://esr.ibiblio.org/?p=538][The Unix Hater’s Handbook, Reconsidered]]
**** [[http://www.catb.org/esr/writings/taoup/][The Art of Unix Programming]], by /Eric S. Raymond/                 :book:
**** [[http://mywiki.wooledge.org/BashFAQ][BashFAQ]] and [[http://mywiki.wooledge.org/BashPitfalls][BashPitfalls]]                                         :shell:
**** [[http://harmful.cat-v.org/software/dynamic-linking/][Dynamic linking]]
In particular, see this comparison around [[http://port70.net/~nsz/32_dynlink.html][shared libraries and dynamic linking]].
While I believe this used to be a valid case, much of it is severly invalidated
by functional operating systems like NixOS or Guix System.

**** [[http://doc.cat-v.org/bell_labs/utah2000][Systems Software Research is Irrelevant (aka utah2000 or utah2k)]]    :os:
I believe this paper is fallacious and backward-thinking.  The content is more
in the line of a conservative political agenda than a scientific discussion.  In
fact, it was proven wrong just a few years later with the publication of the
paper on NixOS.

Also see my [[../guix-advance/index.org][article on the Guix advance]].

**** [[http://www.over-yonder.net/~fullermd/rants/bsd4linux/01][BSD vs Linux]]                                                        :os:
This article is a showcase (as is the FreeBSD handbook) of an ideology
widespread in the BSD communities.  I believe this ideology to be driven more by
tribalism than the desire to make scientific advances, which is probably
detrimental to the development of some BSD oeprating systems.

**** [[http://harmful.cat-v.org/][cat-v.org]]                                                         :unix:
Sometimes enlightening, often ill-advised and always insolent.

In http://harmful.cat-v.org/software/ Acme ([[https://research.swtch.com/acme][a mouse driven editor]]), sam and ed
(a commandline text editor) are considered better alternatives than Emacs and
Vim.  This seems hard to defend on almost every level and no arguments are
offered.

If this claim could be proven false one way or another, this would translate a
strong tribal attitute in the author's writings.

On the one hand, cat-v can push to question ourselves, but considering the
boldness of the claims this can only be done with a grain of salt.  On the other
hand, it's a great showcase of how tribalism may lead software developers and
computer scientist to delude themselves and waste their precious time.

**** [[http://suckless.org/philosophy][Suckless philosophy]]                                               :unix:
While some bits are interesting in terms of software engineering, it sometimes
displays signs of stubborn anti-GNU / pro-Unix behaviour.  In particular, from
http://suckless.org/sucks/:

#+BEGIN_QUOTE
Documentation

Somewhen GNU tried to make the world a bit more miserable by inventing
texinfo. The result is that in 2016 man pages are still used and the
documentation of GNU tools requires you to run info $application. The info
browser is awkward and unintuitive and the reason why noone gets further than
finding ‘q’ to quit it.

[…]

The suckless way is to have a short usage and a descriptive manpage. The complete details are in the source.
#+END_QUOTE

All arguments are flawed:

- It's not because something is still around that it's good.  Counter-examples:
  Windows, Fortran.

- There is no measure of the "intuitiveness" of =man= or =info=.  Actually both
  have similar key bindings and display in a terminal emulator by default.

- The "details" in the source implies that the reader would be able to read the
  source code in any language.  I don't believe this is something reasonable to
  expect from any programmer, let alone any user who cannot program.

**** [[https://wiki.archlinux.org/index.php/The_Arch_Way][The Arch Way]]
Arch Linux is a flagship Unix distribution and as such a household of
Unix philosophy defenders.  I believe much of the principles represent the sane
part of the Unix philosophy and make up for one of the best Unix distributions
out there.

* Operating systems and freedom
**** [[https://www.getgnulinux.org/en/windows/][Why not Windows]]                                    :security:os:freedom:
**** [[https://www.gnu.org/proprietary/malware-microsoft.en.html][Microsoft's Software is Malware]]                    :security:freedom:os:
**** [[https://mikegerwitz.com/2016/04/GNU-kWindows][GNU/kWindows]]                                               :security:os:
**** [[https://web.archive.org/web/20010801155417/www.unix-vs-nt.org/kirch][Microsoft Windows NT Server 4.0 versus UNIX]]                 :os:history:
**** [[https://nullprogram.com/blog/2018/04/13/][Blast from the Past: Borland C++ on Windows 98]]    :history:os:languages:

* Unix books you'd rather not read
There is much to say as to how the following seminal books have had a
detrimental impact in the evolution of computer science.  This discussion needs
a dedicated article.

**** The C Programming Language, by /Brian W. Kernighan & Dennis M. Ritchie/ :book:
**** The UNIX Programming Environment, by /Brian W. Kernighan & Rob Pike/ :book:

* User interfaces
**** [[http://contemporary-home-computing.org/RUE/][Rich User Experience, UX and Desktopization of War]]           :education:

* Emacs
**** [[https://www.gnu.org/software/emacs/emacs-paper.html][EMACS: The Extensible, Customizable Display Editor]]               :emacs:
**** [[https://www.quora.com/Does-Emacs-violate-the-UNIX-philosophy-of-doing-one-thing-very-well/answer/Tikhon-Jelvis][Does Emacs violate the UNIX philosophy of doing one thing very well?]], by /Tikhon Jelvis/ :emacs:
**** [[https://www.quora.com/Is-Emacs-evil][Is Emacs evil?]]                                                   :emacs:
**** [[http://tess.oconnor.cx/2006/03/quality-without-a-name][Emacs: the 100-year editor]]                                       :emacs:
**** [[http://emacshorrors.com/posts/index.html][Emacs horrors]]                                                    :emacs:
**** [[https://github.com/emacs-tw/awesome-emacs][Awesome Emacs]]                                                    :emacs:
**** [[http://planet.emacsen.org/][Planet Emacsen]]                                                   :emacs:
**** [[http://emacs.readthedocs.io/en/latest/][Emacs RTD]]                                                        :emacs:
**** [[http://orgmode.org/guide][The compact Org-mode Guide]]                                   :emacs:org:
**** [[https://oremacs.com/][(or emacs irrelevant)]]                                            :emacs:
**** [[https://www.jwz.org/doc/lemacs.html][The Lemacs/FSFmacs Schism.]]                               :emacs:history:
**** [[http://www.howardism.org][Howardism]]                                                        :emacs:
***** [[http://www.howardism.org/Technical/Emacs/eshell-fun.html][Eschewing Zshell for Emacs Shell]]                          :emacs:shell:
***** [[http://www.howardism.org/Technical/Emacs/new-window-manager.html][Emacs is My New Window Manager]]                                  :emacs:
**** [[https://technomancy.us/184][in which a path is charted through the coming apocalypse]]    :exwm:emacs:
**** [[https://elephly.net/posts/2016-02-14-ilovefs-emacs.html][Why GNU Emacs?]]                                                   :emacs:

* Programming languages
**** [[http://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD831.html][Why numbering should start at zero]], by /E. W. Dijkstra/      :languages:
**** [[http://www.csis.pace.edu/~bergin/patterns/ppoop.html][Understanding Object Oriented Programming]]   :computing:humour:languages:
**** [[https://prog21.dadgum.com/156.html][OOP Isn't a Fundamental Particle of Computing]]                :languages:
**** [[https://transitiontech.ca/random/RIIR][Have you considered Rewriting It In Rust?]]                    :languages:
**** [[http://yosefk.com/c++fqa/index.html][C++ FQA]]                                                      :languages:
**** [[https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern][Curiously recurring template pattern]]                         :languages:
**** [[https://www.jwz.org/doc/java.html][java sucks.]]                                                  :languages:
**** [[http://projects.haykranen.nl/java][Enterprisify your Java Class Names!]]                   :humour:languages:
**** [[https://en.wikipedia.org/wiki/Qrpff][Qrpff]]                                                        :languages:
**** [[http://doc.cat-v.org/bell_labs/pikestyle][Notes on Programming in C]], by /Rob Pike/                     :languages:
**** [[https://www.youtube.com/watch?v=Sg4U4r_AgJU][Brian Kernighan on successful language design]]                :languages:
Interesting to see that one of the most famous figures behind Unix and the C
programming language admits to struggle with C :p
**** [[http://commandcenter.blogspot.com.au/2012/04/byte-order-fallacy.html][The byte order fallacy]]                                       :languages:
**** [[http://commandcenter.blogspot.com.au/2012/06/less-is-exponentially-more.html][Less is exponentially more]]                                   :languages:

* Lisp and programming language theory
**** [[https://www.youtube.com/watch?v=OyfBQmvr2Hc][The Most Beautiful Program Ever Written]] by /William Byrd/         :lisp:
**** [[https://www.youtube.com/watch?v=svmPz5oxMlI][Lisp, The Quantum Programmer's Choice]]                       :video:lisp:
**** [[http://norvig.com/Lisp-retro.html][A Retrospective on /Paradigms of AI Programming/]]                  :lisp:
**** [[http://smuglispweeny.blogspot.com/2008/02/ooh-ooh-my-turn-why-lisp.html][Ooh! Ooh! My turn! Why Lisp?]]                                      :lisp:
**** [[http://sds.podval.org/tool.html][The Right Tool For The Job]]                                        :lisp:
**** [[https://redditblog.com/2005/12/05/on-lisp/][on lisp]] (Reddit blog)                                             :lisp:
**** [[https://lispcast.com/idea-of-lisp/][The Idea of Lisp]]                                                  :lisp:
**** [[https://www.reddit.com/r/lisp/comments/7lf149/what_can_other_languages_do_that_lisp_cant/][What can other languages do that Lisp can't?]]                      :lisp:
**** [[https://www.quora.com/Why-isnt-the-graphical-programming-environment-Racket-DrScheme-more-popular][Why isn't the graphical programming environment Racket (DrScheme) more popular?]] :lisp:racket:
**** [[https://github.com/racket/racket/wiki/Friends-of-Racket][Friends of Racket]]                                          :lisp:racket:
**** [[http://pchristensen.com/blog/lisp-companies/][Lisp Companies]]                                                    :lisp:
**** [[https://www.quora.com/Why-is-Haskell-not-homoiconic][Why is Haskell not homoiconic?]]                         :lisp:haskell:ml:
**** [[http://sds.podval.org/ocaml-sucks.html][OCaml Language Sucks]]                                     :lisp:ml:ocaml:
***** [[https://www.reddit.com/r/lisp/comments/5wk7e0/ocaml_sucks_and_some_comparison_to_lisp/][Ocaml sucks (and some comparison to Lisp)]]               :lisp:ml:ocaml:
**** [[https://xkcd.com/224][xkcd: Lisp]]                                                 :lisp:humour:
**** [[https://xkcd.com/297/][xkcd: Lisp Cycles]]                                          :lisp:humour:
**** [[http://wingolog.org/archives/2013/01/07/an-opinionated-guide-to-scheme-implementations][an opinionated guide to scheme implementations]]             :lisp:scheme:
**** Lisp, by Paul Graham
***** [[http://www.paulgraham.com/avg.html][Beating the Average]] April 2001, rev. April 2003
***** [[http://www.paulgraham.com/rootsoflisp.html][The Roots of Lisp]] May 2001                               :lisp:history:
***** [[http://www.paulgraham.com/popular.html][Being Popular]] May 2001                                   :lisp:history:
***** [[http://www.paulgraham.com/icad.html][Revenge of the Nerds]] May 2002
Where Paul Graham argues about why code size matters, with examples in Perl,
Python, Smalltalk and others.

The article embeds most of [[http://www.paulgraham.com/diff.html][What Made Lisp Different]] (December 2001,
rev. May 2002) but for a few details.
***** [[http://www.paulgraham.com/power.html][Succinctness is Power]] May 2002
***** [[http://paulgraham.com/hundred.html][The Hundred-Year Language]] April 2003
***** [[http://www.paulgraham.com/iflisp.html][If Lisp is So Great]] May 2003
**** [[https://www.quora.com/How-do-I-solve-a-problem-using-dynamic-programming-approach-and-which-function-should-I-use-recursively-using-loops/answer/Panicz-Godek][Panicz Godek's answer to How do I solve a problem using dynamic programming approach, and which function should I use recursively using loops?]] :lisp:scheme:algorithms:
**** [[https://www.quora.com/What-are-the-pros-and-cons-of-functional-programming-compared-to-imperative-programming/answer/Panicz-Godek][Panicz Godek's answer to What are the pros and cons of functional programming compared to imperative programming?]] :lisp:scheme:commonlisp:
**** [[https://www.quora.com/What-lasting-effects-did-learning-LISP-have-on-you/answer/Panicz-Godek][Panicz Godek's answer to What lasting effects did learning LISP have on you?]] :lisp:

* Lisp literature
**** [[https://mitpress.mit.edu/sites/default/files/sicp/index.html][Structure and Interpretation of Computer Programs]], by /Abelson, Sussman and Sussman/ :lisp:scheme:book:
This book introduces computer programming from the angle of functional
programming.  Well written, it teaches important core principles around
architecture, non-leaky abstractions, recursion, higher-order functions, the cost of affectation,
objects and dispatch, the fundamental pros and cons of laziness, and a lot
more.

The biggest take-away of this book, in my opinion, is that /traversable layers
of abstraction/ are key to architecture all computer knowledge.

Also available in Texinfo format, either online
https://sarabander.github.io/sicp/ or as the Guix package =sicp=.

The video recording of the lectures by the authors is available:
https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-001-structure-and-interpretation-of-computer-programs-spring-2005/video-lectures/

The book was criticized in [[https://www2.ccs.neu.edu/racket/pubs/jfp2004-fffk.pdf][The Structure and Interpretation of the Computer
Science Curriculum]].  The critiques have published [[https://htdp.org/2020-5-6/Book/index.html][How to Design Programs]] as an
answer.

**** [[http://landoflisp.com/][Land of Lisp]]                                                 :lisp:book:
The homepage contains an rather surprising comic strip introducing many of the
unique assets of the Lisp languages.

**** [[http://people.ace.ed.ac.uk/staff/medward2/class/moz/cm/doc/contrib/lispstyle.html][Lisp Style Tips for the Beginner]]                       :lisp:commonlisp:
**** [[http://mumble.net/~campbell/scheme/style.txt][Riastradh's Lisp Style Rules]]                               :lisp:scheme:
**** [[http://jakob.space/blog/thoughts-on-lisps.html][The Many Faces of an Undying Programming Language]]                 :lisp:

* Common Lisp literature
**** [[https://lispcookbook.github.io/cl-cookbook/][The Common Lisp Cookbook]]                                    :commonlisp:
**** [[http://stevelosh.com/blog/2018/08/a-road-to-common-lisp/][A Road to Common Lisp]]                                       :commonlisp:
**** [[http://www.gigamonkeys.com/book/][Practical Common Lisp]]                                  :book:commonlisp:
**** [[https://norvig.github.io/paip-lisp/][Paradigms of Artificial Intelligence Programming]], by /Peter Norvig/ :book:commonlisp:
**** [[http://www.paulgraham.com/onlisp.html][On Lisp]], by /Paul Graham/                              :book:commonlisp:
**** [[https://awesome-cl.com/][awesome-cl]]                                                  :commonlisp:
**** [[http://www.nhplace.com/kent/Papers/cl-untold-story.html][Common Lisp: The Untold Story]]                               :commonlisp:

* Scheme literature
**** https://schemers.org/                                           :scheme:
**** [[https://www.scheme.com/tspl4/][The Scheme Programming Language]], by  /R. Kent Dybvig/      :scheme:book:
**** [[http://www.r6rs.org/][The Revised6 Report on the Algorithmic Language Scheme]]          :scheme:
**** https://www.gnu.org/software/guile/learn/                       :scheme:
**** [[https://www.gnu.org/software/guile/manual/][GNU Guile Reference Manual]]                                 :scheme:book:
**** [[https://www.cs.indiana.edu/~dyb/pubs/tr356.pdf][Writing Hygienic Macros in Scheme with Syntax-Case]]              :scheme:

* Racket literature
**** [[https://docs.racket-lang.org/][Racket Documentation]]                                            :racket:
**** [[http://felleisen.org/matthias/manifesto/][The Racket Manifesto]]                                            :racket:
**** [[https://htdp.org/][How to Design Programs]]                                     :racket:book:
**** [[http://world.cs.brown.edu/][How to Design Worlds]]                                       :racket:book:
**** [[http://www.greghendershott.com/fear-of-macros/][Fear of Macros]]                                                  :racket:
**** [[http://www.bootstrapworld.org/][Bootstrap]]                                                       :racket:

* Regular expressions
**** [[https://commandcenter.blogspot.in/2011/08/regular-expressions-in-lexing-and.html][Regular expressions in lexing and parsing]]                       :regexp:
**** [[https://swtch.com/~rsc/regexp/regexp1.html][Regular Expression Matching Can Be Simple And Fast]]              :regexp:
**** [[http://francismurillo.github.io/2017-03-30-Exploring-Emacs-rx-Macro/][Exploring Emacs rx Macro]]                             :emacs:lisp:regexp:
**** [[http://docs.racket-lang.org/peg/index.html][Racket's PEG library]]                                     :racket:regexp:

* Algorithms
**** [[https://en.wikipedia.org/wiki/The_Art_of_Computer_Programming][The Art or Computer Programming]], by Donald E. Knuth    :book:algorithms:
**** [[http://cryptopals.com][the cryptopals crypto challenges]]                          :cryptography:
**** [[http://projecteuler.net/problems][Project Euler]]                                               :algorithms:
**** [[http://www.nada.kth.se/~viggo/problemlist/compendium.html][A compendium of NP optimization problems]]                  :cryptography:
**** [[https://en.wikibooks.org/wiki/Algorithm_Implementation/][Algorithm Implementation Wikibook]]                      :book:algorithms:

* Computer graphics and gaming
**** [[http://cowboyprogramming.com/2007/01/05/evolve-your-heirachy/][Evolve Your Hierarchy]]                                 :languages:gaming:
**** [[http://www.kbs.twi.tudelft.nl/Publications/MSc/2001-VanWaveren-MSc.html][The Quake III Arena Bot]]                                    :gaming:book:
**** [[https://technomancy.us/187][in which a game jam is recounted]]                           :lisp:gaming:
**** [[https://prog21.dadgum.com/23.html][Purely Functional Retrogames]]                                    :gaming:
**** [[http://fabiensanglard.net/][Fabien Sanglard's Website]]                                       :gaming:
**** [[http://gamesfromwithin.com/data-oriented-design][Data-Oriented Design (Or Why You Might Be Shooting Yourself in The Foot With OOP)]] :gaming:
**** [[https://www.technologizer.com/2011/12/11/computer-space-and-the-dawn-of-the-arcade-video-game/][Computer Space and the Dawn of the Arcade Video Game]]     :games:history:
**** [[http://www.codeofhonor.com/blog/tough-times-on-the-road-to-starcraft][Tough times on the road to Starcraft]]                     :games:history:
**** [[https://cdn.arstechnica.net/wp-content/uploads/2018/02/a3621d29-765c-4e1a-a4e8-2598b68b24b4mezzanine.txt][I'm Paul Neurath. I oversaw the creation of Thief]]        :games:history:
**** [[https://www.beyond3d.com/content/articles/8][Origin of Quake3's Fast InvSqrt()]]             :games:history:algorithms:

* Book listings
Mostly around computing but not exclusively.

**** [[https://github.com/EbookFoundation/free-programming-books][List of Free Learning Resources]]
**** [[https://stackoverflow.com/questions/1711/what-is-the-single-most-influential-book-every-programmer-should-read][What is the single most influential book every programmer should read?]]
**** [[http://www.realtimerendering.com/#books][Real-Time Rendering Resources]]                          :gaming:graphics:
**** [[https://technomancy.us/books][Books]], by /Phil Hagelberg/ (Technomancy)
**** [[http://mrelusive.com/books/books.html][Books For Game Developers]], by /J.M.P. van Waveren/              :gaming:
**** [[http://peterwonka.net/Documentation/BooksToRead.htm][Books To Read]], by /Peter Wonka/                        :graphics:gaming:
**** [[https://old.reddit.com/r/Common_Lisp/comments/ddcoar/selection_of_lisp_books/][Selection of Lisp books]]                                     :lisp:books:

* Frivolities
**** [[http://kmkeen.com/awk-music/][AWK music]]
**** [[http://wiki.c2.com/?FeynmanAlgorithm][Feynman Algorithm]]                                               :humour:
**** [[http://foaas.com/Everything/JonLaJoie][FOASS]]                                                           :humour:
**** [[http://wilwheaton.net/2016/03/fuck-daylight-saving-time][Fuck Daylight Saving Time]]
**** [[https://en.wikipedia.org/wiki/G%C3%B6mb%C3%B6c][Gömböc]]
**** [[https://en.wikipedia.org/wiki/Monkey_selfie_copyright_dispute][Monkey selfie copyright dispute]]
**** [[https://en.wikipedia.org/wiki/Non-rocket_spacelaunch][Non-rocket spacelaunch]]
**** [[https://en.wikipedia.org/wiki/Office_Assistant][Office Assistant]]                                      :humour:computing:
**** [[https://rationalwiki.org/wiki/Security_theater][Security theater]]                                       :politic:freedom:
**** [[https://en.wikipedia.org/wiki/Uncontacted_peoples][Uncontacted peoples]]
**** [[https://en.wikipedia.org/wiki/Useless_machine][Useless machine]]
