#+TITLE: Cinema: Suicide Club
#+SUBTITLE: By Manuel-Hoerth

Posted: Thu Oct 13 2011 21:23:43.

Post Edited: Fri Oct 14 2011 09:00:58.

Originally from http://www.imdb.com/title/tt0312843/board/nest/189698427.

* For those who after watching it couldn't figure it out

So you watched the movie and where like me and thought "What the heck"? I have
to admit i haven't figured out everything either, as i am too lazy to watch the
movie multiple times myself - if i where to do that i am sure i could see even
more things. But anyway i think i figured the main thing out that everyone was
wondering about, namely the answer to the "what the heck is going on" -question.

Strangely nobody has actually posted it so far - instead the posts i read on
here so far are actually pretty misleading like saying "it's a Japanese thing
you can't understand it from a western perspective" or "it's a Buddhist thing"
or whatever... that's BS. And actually that's precisely the reason why nobody
has figured it out cause we are all expecting some esoteric eastern
explanation... and so we start to think much too complicated and we go offtrack.
Sure it's a Japanese movie, but have you noticed that the Japanese are actually
even bigger fans of western culture than we are of eastern culture? Why else
would they try to speak English all the time or have references to the Rocky
Horror Picture Show?

So here goes the spoiler: Yes, it's a cultural thing... and yes it's true that
only people with a certain cultural background can understand it. But the
cultural background required is not Japanese, it's German! To put it blunt, this
movie is nothing more than a gory, modernized, Japanese version of the Pied
Piper! Think about it. Watch the movie two times and you will notice it... i
watched it only one time, but i am German so naturally i am familiar with the
legend and noticed it on my first watch. The policeman even mentions it in the
beginning. Later on you have that creepy dude who appears to be the manager or
personal child molester of the "Dessert" kids (-: You see him towards the end of
the movie with a black mask on his head, together with all the kids and cutting
their skin away and there are tons of chickens on the floor too that at that
moment actually look and squeak like rats. This creepy guy is the Pied Piper! He
uses the secret code you see in the poster to lure the kids in. And only
children can learn his secret code (simple because only kids would be obsessed
enough with "Dessert" to actually figure the code out).

Once they have the code and use it on the door to the backstage area they are
basically brainwashed into committing suicide as you can see with the girl at
the end or with the policemen who is made to commit suicide. There are MANY
references to the Pied Piper... probably much more than what i spotted. And just
like in the Pied Piper, he first makes the children follow him using his music
and his magic (in this case J-Pop and his "magic" code) then he leads the
children away, and then he leads them to their death. Even the method of death
is the same. in the Pied Piper he makes them jump down a cave in the hills. In
"Suicide Club" he makes them jump down a school, or some other building or makes
them jump down the subway... anyway they always jump down something and always
collectively, just like in the Pied Piper. Also the subway itself could simply
be a modernized version of the cave in the original German story. Also the
subway is located just outside Tokyo just like the cave is located just outside
Hamelin. Also like in the Pied Piper the adults are to blame for not watching
over their children. In the Pied Piper the adults attend church and leave the
kids unsupervised. In Suicide Club the police (who are also the adults and the
parents) try hard to solve the case. But they don't pay any attention to their
kids and family and it's precisely that lack of attention for their children
that prevents them from actually solving the case and from protecting their
children.

PS: I spotted yet another Pied Piper reference. In the Pied Piper there is one
kid who is able to tell the adults about it because he is blind and crippled and
so he couldn't catch up with the others - but precisely because he is blind he
couldn't see where they went. You have the same thing in Suicide Club where the
girl calling herself "the bat" tries to inform the police while being
blindfolded. You can even see that she is unable to describe where she is
precisely because she can't see so that wanna be Charles Mansion types it for
her. And the fact that she is put in a sack makes her move around like being
crippled...

And yet another one: in the first video we see of "Dessert" when they are
wearing the clothes with the numbers on them that are relevant for the secret
code... those clothes consist of many brightly colored stripes (pied)... just
like the colorful stripes the pied piper is wearing in the original German
legend.

And yet another one: as someone already pointed out "Dessert" is sometimes
spelled "Dessert" and sometimes "Desert" in the movie. So this is another hint
to how the parents have deserted their children making it possible for the Pied
Piper to exploit that carelessness just like he does in the original story.

And yet another one... man once you know what to look for it really never
stops... i was wondering about the dates...the movie always tells you what date
it is and i couldn't figure out the significance of that. Also we are only told
the day and the month - not the year. The film starts on May 26th with the
subway suicides. In the original story the Pied Piper starts leading the
children away on June 26th. I know it a month off, but it's the 26th nonetheless
so i don't think it's a coincidence.
