#+TITLE: Mastering the keyboard
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../dark.css" />
#+DATE: <2016-02-04 Thu>

The following article deals with techniques to optimize comfort and speed using
a computer.

* Touch typing

Writing text holds a central part in our use of computers: e-mails, searching,
web browsing, programming, configuring, etc.

Touch typing is the art of typing (fast) without having to look at the keyboard.
This is a skill that is surprisingly left aside, even by professionals.  I
believe it to be a big time saver, while alleviating the frustration of too much
stuttering and too many typos.

Touch typing can be trained in a fairly short amount of time.  One way would be
to use a trainer program such as [[https://www.gnu.org/software/gtypist/][GNU Typist]].  It is straightforward to go
through the various lessons and the result will be immediately noticeable.

Touch typing is highly dependent on the keyboard layout (a.k.a. /keymap/), so
you might want to choose the keymap wisely before starting the training.  Read on
for tips on keymaps.

* Mouse-less control

The mouse use has increased tremendously since the rise of graphical user
interfaces.  Mostly for illegitimate reasons.  Do we really need a mouse to select
objects or to toggle buttons?

The mouse proves to be poor at selecting text.  How many times have you tried to
select a sentence and missed the last letter?  It is equally bad at selecting
objects.  When it comes to interact with the user interface, it is usually faster
to use keyboard shortcuts.  Well, we call them "shortcuts" for a reason…

The use of a mouse makes sense when there is a need for a continuous input, such
as in graphics design, video games, etc.

* Home row vs. arrows

The /home row/ refers to the center row of the alphabetical part of the
keyboard, that is, the characters =asdf…jkl;= on a QWERTY keyboard.  The
standard position is when the index fingers rest on the characters =f= and =j=
on a QWERTY keyboard.  Those letters usually come with a bump to make them
distinguishable without looking.

Moving hands from the home row to the arrows back and forth can be a small waste
of time that quickly stacks up.  The time required for the "context switch" of
the hand disturbs the flow.

Arrows tend to be omnipresent when it comes to "regular" text editing or
interface navigation.  Now there are various changes we can make to the
environment so that the navigation bindings stick around the home row.

First of, you may consider switching your text editor for one that allows
navigation without arrows.  Famous examples include /Emacs/ and /Vim/.

The window manager can have limited bindings to such an extent that it forces
the use of arrows or the mouse.  Decent window managers usually feature full
keyboard control.  Popular examples include /Awesome/ and /i3/.

Web browsers have become more and more dominant in our use of computers.  The way
the World Wide Web was designed has put emphasis on the mouse, so that it is now
almost impossible to browse the web without a mouse.  Which might be a sign for
poor design from the ground up.  But let's not drift off too much.  It is still
possible to use a graphical web browser while making best use of the keyboard
thanks to the "hint" feature.  Many Webkit-based browsers offer this feature.  It
is also possible to edit any field using your favourite editor, which greatly
alleviates the need for a mouse and arrows.

If you have got the chance to witness a hardcore geek with proper touch typing
skills and a keyboard / home row centered environment, you will be amazed by how
many actions per minute that geek can perform!

* Caps-Lock switch

The Caps-Lock key is very accessible albeit little used.  On the other hand, the
use frequency of keys such as =Control= or =Escape= is much higher (in
particular when using the Emacs or Vim text editors).

Therefore it is very recommended to swap Caps-Lock with the key you use most.
There are several ways of doing this, read on for an example.

This is one of the keymap tweaks that will save you most from wrecking your
hands with some carpal tunnel syndrome.

* International custom keymaps

Users of languages using a Latin-based alphabet should be familiar with the
existence of a variety of "standard" keymaps out there: QWERTY (US, UK…),
QWERTZ, AZERTY, to name a few.

If you find yourself writing in more than one language, you will often find the
need to switch keymap so that you can write some special characters easily.  This
is a big mistake, as the context switch between the various layouts can be
extremely disturbing and require minutes if not hours each time before feeling
comfortable again.

Letters and punctuation often vary between keymaps.  (AZERTY and QWERTY are good
examples of this.)  While additional special characters are welcome, the
positional alteration of standard characters is not strictly necessary.  So what
if we would have a keymap that contains special characters for various languages
at the same time?  There is no such standard keymap, but it is possible to create
one yourself.

Using one single custom keymap has the advantage of eliminating the context
switch disturbance while providing direct access to all the desired special
characters.  Besides, it is possible to base the new keymap on QWERTY US, which
has some inherent benefits:

- Matching parentheses are next to each other.  Punctuation tends to be
  reasonably accessible (e.g. =,= and =.= are unshifted).

- It is the most widespread keymap, so when somebody wants to use your computer,
  chances are high they can type something.

- Most importantly, some programs are ergonomically optimized for QWERTY US,
  such as Emacs and Vim.

Bonus for scientists: it is possible to add some common mathematical characters,
such as =≠= or =⋅=, which can be a big time saver when it comes to writing
scientific documents.

** Custom Xkb keymaps

Let's move on to the details on how to load a custom layout for the X window
system without needing administrative rights.

In the following, we will use the =xkb= folder as our workspace.  This folder is
arbitrary.  Replace the keymap name =custom= with any unused name you like.

Create an =xkb/custom.xkb= file:

#+BEGIN_EXAMPLE
xkb_keymap {
    xkb_keycodes  { include "evdev+aliases(qwerty)" };
    xkb_types     { include "complete" };
    xkb_compat    { include "complete" };
    xkb_symbols   { include "pc+custom+inet(evdev)" };

    // Geometry is completely optional.
    // xkb_geometry  { include "pc(pc104)"  };
};
#+END_EXAMPLE

Fill in =xkb/symbols/custom=.  This file is formatted just like every other Xkb
symbol files, usually located in =X11/xkb/symbols/= under =/usr/share= or
=/usr/local/share=.

Finally, load the new keymap with

#+BEGIN_SRC sh
$ xkbcomp -I"xkb" "xkb/custom.xkb" $DISPLAY
#+END_SRC

For a concrete example, see [[https://gitlab.com/ambrevar/dotfiles/tree/master/.xkb/][my personal keymap]].  It is a superset of QWERTY US
which covers almost every language in western Europe, with Caps-Lock and Control
swapped.
