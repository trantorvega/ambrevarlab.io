;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode
  (org-texinfo-classes ("guix"
                        "@documentencoding AUTO\n@documentlanguage AUTO"
                        ("@subsection %s" "@unnumberedsubsec %s" "@subheading %s"
                         "@appendixsubsec %s")
                        ("@subsubsection %s" "@unnumberedsubsubsec %s" "@subsubheading %s"
                         "@appendixsubsubsec %s")))))
