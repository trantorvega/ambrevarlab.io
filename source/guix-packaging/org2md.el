(require 'ox-md)

;; TODO: Export source code blocks without language tag and unindented.

(defvar org2md-input "index.org")
(defvar org2md-output (replace-regexp-in-string "\\.org$" ".md" org2md-input))

(defun org2md-replace-all (string replace)
  (save-excursion
    (goto-char (point-min))
    (while (search-forward string nil 'noerror)
      (replace-match replace))))

;; This is a specialization of `org-publish-find-property'.
(defun org2md-find-properties (file &rest properties)
  "Find the PROPERTIES of FILE in project.

PROPERTIES is a keyword referring to an export option, as defined
in `org-export-options-alist' or in export back-ends.

  (org2md-find-properties file :subtitle)

Return value may be a string or a list, depending on the type of
PROPERTIES, i.e. \"behavior\" parameter from `org-export-options-alist'."
  (when (and (file-readable-p file) (not (directory-name-p file)))
    (let* ((org-inhibit-startup t)
           (visiting (find-buffer-visiting file))
           (buffer (or visiting (find-file-noselect file))))
      (unwind-protect
          (let ((result))
            (dolist (property properties result)
              (setq result (plist-put
                            result
                            property
                            (plist-get (with-current-buffer buffer
                                         (if (not visiting)
                                             (org-export-get-environment 'md)
                                           ;; Protect local variables in open buffers.
                                           (org-export-with-buffer-copy
                                            (org-export-get-environment 'md))))
                                       property)))))
        (unless visiting (kill-buffer buffer))))))

(defun org2md-insert-heading (input)
  (let ((props
         (org2md-find-properties org2md-input :title :date :author :select-tags)))
    (goto-char (point-min))
    (insert (format
             "title: %s
date: %s
author: %s
tags: %s
---
"
             (car (plist-get props :title))
             (org-timestamp-format (car (plist-get props :date)) "%Y-%m-%d")
             (car (plist-get props :author))
             (mapconcat 'identity (plist-get props :select-tags) " ")))))

(defun org2md-set-language-tags ()
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "\n\n    +" nil 'noerror)
      (let ((language-code (pcase (char-after)
                             (?$ "sh")
                             (?> "scheme")
                             (_ "scheme"))))
        (forward-line -1)
        (newline)
        (insert (format "```%s" language-code)))
      (re-search-forward "\n\n+[^ ]")
      (goto-char (match-beginning 0))
      (newline)
      (insert "```"))))

(defun org2md-export ()
  (let ((backup-inhibited t))
    (let* ((visiting (find-buffer-visiting org2md-input))
           (buffer (or visiting (find-file-noselect org2md-input))))
      (with-current-buffer buffer
        (let ((org-export-with-toc nil)
              (org-src-preserve-indentation t))
          (org-md-export-to-markdown)))
      (unless visiting (kill-buffer buffer)))
    (with-temp-file org2md-output
      (insert-file-contents-literally org2md-output)
      (org2md-insert-heading org2md-input)
      (org2md-replace-all "<sub>" "_")
      (org2md-replace-all "</sub>" "")
      (org2md-set-language-tags))))
